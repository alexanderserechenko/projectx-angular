import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';



import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { CarouselComponent } from './carousel/carousel.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { ModalModule } from 'ngx-bootstrap';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { HouseDetailsComponent } from './houses/house-details/house-details.component';
import { HouseListComponent } from './houses/house-list/house-list.component';
import { DataTablesModule } from 'angular-datatables';
import { AccessDeniedComponent } from './error/access-denied/access-denied.component';
import { AuthGuardService } from './guards/auth-guard.service';
import {  JwtInterceptor} from './helpers/jwt.interceptor';
import { ReviewComponent } from './review/review.component';
import { AddModalComponent } from './modals/add-modal/add-modal.component';
import { HouseService } from './services/house.service';
import { UpdateModalComponent } from './modals/update-modal/update-modal.component';
import { DeleteModalComponent } from './modals/delete-modal/delete-modal.component';
import { HouseSelectComponent } from './houses/house-select/house-select.component';
import { AdminComponent } from './admin/admin.component';
import { ModalService } from './services/modal.service';
import { FormControlsService } from './services/form-controls.service';
import { UploadComponent } from './upload/upload.component';
import { FileSelectDirective } from 'ng2-file-upload';
import { ContactModalComponent } from './modals/contact-modal/contact-modal.component';
import { MessagesComponent } from './messages/messages.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { AgmCoreModule } from '@agm/core';
import { DeleteMessageComponent } from './messages/delete-message/delete-message.component';









@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    CarouselComponent,
    HouseDetailsComponent,
    HouseListComponent,
    AccessDeniedComponent,
    ReviewComponent,
    AddModalComponent,
    UpdateModalComponent,
    DeleteModalComponent,
    HouseSelectComponent,
    AdminComponent,
    UploadComponent,
    FileSelectDirective,
    ContactModalComponent,
    MessagesComponent,
    DeleteMessageComponent,
    

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CarouselModule.forRoot(),
    ReactiveFormsModule,
    HttpClientModule ,   
    FormsModule,
    NgbModule.forRoot(),
    ModalModule.forRoot(),
    DataTablesModule,
    CKEditorModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDLpG8yyX-EJVQ5bvQ9tioIo2OoHWUIaxw'
    })
    


    

   
   



  ],
  providers: [
    HouseService,
    ModalService,
    AuthGuardService,
    FormControlsService,
    HouseSelectComponent,
    UpdateModalComponent,
    DeleteModalComponent,
    AddModalComponent,
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
