import { Component, OnInit, ViewChild, Input, ChangeDetectorRef } from '@angular/core';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { HouseService } from 'app/services/house.service';
import { ModalService } from 'app/services/modal.service';
import { AccountService } from 'app/services/account.service';
import { ConfigService } from 'app/services/config.service';



@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {


  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

@Input() hId: number;

@ViewChild(DataTableDirective) dtElement: DataTableDirective;

  constructor(
    public modalProp: ModalService,
    private houseService: HouseService,
    private chRef: ChangeDetectorRef,
    private acct: AccountService,
    public configService: ConfigService
    ) { }

  ngOnInit() {

    this.dtOptions = {
      paging: false,
      autoWidth: true,
      lengthChange: false,
      searching: false,

      };

    this.modalProp.houses$ = this.houseService.getHouses();
    this.modalProp.houses$.subscribe(result => {

        this.modalProp.houses = result;
        this.chRef.detectChanges();
        this.dtTrigger.next();

      });

      this.acct.currentUserRole.subscribe(result => {this.modalProp.userRoleStatus = result});


  }

  public createImgPath = (serverPath: string) => {
    const sPath = serverPath.split(',');
    return `https://localhost:5001/upload/${sPath[0]}`;
  }

  ngOnDestroy() {
      // Do not forget to unsubscribe
      this.dtTrigger.unsubscribe();
  }


}
