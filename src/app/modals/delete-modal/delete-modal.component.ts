import { Component, OnInit, ViewChild, TemplateRef, Input } from '@angular/core';
import { HouseService } from 'app/services/house.service';
import { Ihouse } from 'app/interfaces/Ihouse';
import { BsModalService } from 'ngx-bootstrap';
import { ModalService } from 'app/services/modal.service';
import { FormControlsService } from 'app/services/form-controls.service';

@Component({
  selector: 'app-delete-modal',
  templateUrl: './delete-modal.component.html',
  styleUrls: ['./delete-modal.component.css']
})
export class DeleteModalComponent implements OnInit {

  @ViewChild('deleteTemplate') deletemodal: TemplateRef<any>;

  @Input() hId: Ihouse;


  constructor(
    public fc: FormControlsService,
    public modalProp: ModalService,
    private houseService: HouseService,
    private modalService: BsModalService

    ) { }

  ngOnInit() {
  }



   // Method to Delete the house
   onDelete(): void {
    const deleteHouse = this.hId;

    this.houseService.deleteHouse(deleteHouse.houseId).subscribe(result => {
    this.houseService.clearCache();
    this.modalProp.houses$ = this.houseService.getHouses();
    this.modalProp.houses$.subscribe(newlist => {
            this.modalProp.houses = newlist;
            this.modalProp.modalRef.hide();
           // this.rerender();
        });
    });
}

onDeleteModal(houseDelete: Ihouse): void {
    this.modalProp.modalRef = this.modalService.show(this.deletemodal);
}




}
