import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';

import { BsModalService } from 'ngx-bootstrap';
import { HouseService } from 'app/services/house.service';

import { ModalService } from 'app/services/modal.service';
import { FormControlsService } from 'app/services/form-controls.service';

import L from 'leaflet';

import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';



@Component({
  selector: 'app-add-modal',
  templateUrl: './add-modal.component.html',
  styleUrls: ['./add-modal.component.css'],

})
export class AddModalComponent implements OnInit {


  constructor(
    public modalProp: ModalService,
    private modalService: BsModalService,
    private houseService: HouseService,
    private fb: FormBuilder,
    public fc: FormControlsService,

    ) { }



    
  public Editor = ClassicEditor;

  public response: {dbPath: ''};
// Add Modal
  @ViewChild('template') modal: TemplateRef<any>;
  ArrayOfImageNames = new Array<string>();

  ngOnInit() {





    this.fc.name = new FormControl('', [Validators.required, Validators.maxLength(50)]);
    this.fc.price = new FormControl('', [Validators.required,Validators.min(0), Validators.pattern(/^[1-9][0-9]{0,2}(?:\.?[0-9]{3}){0,3}(,[0-9]{2})?$/)]      );
    this.fc.description = new FormControl('',[Validators.required] );
    this.fc.imageUrl = new FormControl([new FormControl()]);
    this.fc.latitude = new FormControl('',[Validators.required, Validators.pattern(/^[0-9]*\.?[0-9]*$/)]);
    this.fc.longitude = new FormControl('',[Validators.required, Validators.pattern(/^[0-9]*\.?[0-9]*$/)]);

    this.fc.insertForm = this.fb.group({

            name : this.fc.name,
            price : this.fc.price,
            description : this.fc.description,
            imageUrl : this.fc.imageUrl,
            latitude: this.fc.latitude,
            longitude: this.fc.longitude

            });





  }



  onAddHouse() {
    this.modalProp.modalRef = this.modalService.show(this.modal);

}
  UploadFileNamesChange(arrayOfImages) {
    this.ArrayOfImageNames = arrayOfImages;
  }




 // Method to Add new House
 onSubmit() {
  const newHouse = this.fc.insertForm.value;
  // load the array of houses from the child
  newHouse.imageUrl = this.ArrayOfImageNames.join(',');
  this.houseService.insertHouse(newHouse).subscribe(
      result => {
          this.houseService.clearCache();
          this.modalProp.houses$ = this.houseService.getHouses();

          this.modalProp.houses$.subscribe(newlist => {
              this.modalProp.houses = newlist;
              this.modalProp.modalRef.hide();
              this.fc.insertForm.reset();
              // this.rerender();

              });
          console.log('New House added');

      },
      error => console.log('Could not add House')

      );

}



// upload(){

//   let files = this.fileField.getFiles();
//   console.log(files);
  // let formData = new FormData();
  // for (let i = 0; i < files.length; i++) {
  //     const fileToUpload = files[i];
  //     formData.append('file', fileToUpload, fileToUpload.name);
  //   }
  //   this.http.post('https://localhost:5001/api/upload', formData, {reportProgress: true, observe: 'events'})
  //   .subscribe(event => {
  //     if (event.type === HttpEventType.UploadProgress) {
  //       this.progress = Math.round(100 * event.loaded / event.total);
  //     } else if (event.type === HttpEventType.Response) {
  //       this.message = 'Upload success.';
  //       this.onUploadFinished.emit(event.body);
  //     }
  //   });

  // formData.append('somekey', 'some value') // Add any other data you want to send

  // files.forEach((file) => {
  //   formData.append('files[]', file.rawFile, file.name);
  // });

// }}




// const formData = new FormData();
  // for (let i = 0; i < files.length; i++) {
  //   const fileToUpload = files[i];
  //   formData.append('file', fileToUpload, fileToUpload.name);
  // }
  // // files.forEach(fileToUpload => {
  // //   formData.append('file', fileToUpload, fileToUpload.name);

  // // });
  // this.http.post('https://localhost:5001/api/upload', formData, {reportProgress: true, observe: 'events'})
  // .subscribe(event => {
  //   if (event.type === HttpEventType.UploadProgress) {
  //     this.progress = Math.round(100 * event.loaded / event.total);
  //   } else if (event.type === HttpEventType.Response) {
  //     this.message = 'Upload success.';
  //     this.onUploadFinished.emit(event.body);
  //   }
  // })

}
