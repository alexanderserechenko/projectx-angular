import { Component, OnInit, ViewChild, TemplateRef, Input } from '@angular/core';
import { FormControlsService } from 'app/services/form-controls.service';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import { ModalService } from 'app/services/modal.service';
import { BsModalService } from 'ngx-bootstrap';
import { ContactService } from 'app/services/contact.service';


@Component({
  selector: 'app-contact-modal',
  templateUrl: './contact-modal.component.html',
  styleUrls: ['./contact-modal.component.scss']
})
export class ContactModalComponent implements OnInit {


  @ViewChild('contactTemplate') contactmodal: TemplateRef<any>;
  constructor(
    public fc: FormControlsService,
    public fb: FormBuilder,
    public modalProp: ModalService,
    public modalService: BsModalService,
    public contactService: ContactService

    ) { }

    onSubmit() {

      const newContact = this.fc.contactForm.value;

      this.contactService.insertContact(newContact).subscribe(
      result => {
          this.contactService.clearCache();
          this.modalProp.contacts$ = this.contactService.getContact();

          this.modalProp.contacts$.subscribe(newlist => {
              this.modalProp.contacts = newlist;
              this.modalProp.modalRef.hide();
              this.fc.contactForm.reset();
              // this.rerender();

              });
          console.log('Email has been sent');

      },
      error => console.log('Could not send email')

      );
    }

  ngOnInit() {

    this.fc.nameClient = new FormControl('', [Validators.required, Validators.maxLength(50)]);
    this.fc.emailClient = new FormControl('', [Validators.required, Validators.email]);
    this.fc.messageClient = new FormControl('', [Validators.required]);

    this.fc.contactForm = this.fb.group({

      name : this.fc.nameClient,
      email : this.fc.emailClient,
      message : this.fc.messageClient,
      });

  }



  onContact() {
    this.modalProp.modalRef = this.modalService.show(this.contactmodal);
}

}
