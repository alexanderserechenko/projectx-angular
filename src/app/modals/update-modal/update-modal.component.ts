import { Component, OnInit, ViewChild, TemplateRef,  Input } from '@angular/core';
import { HouseService } from 'app/services/house.service';
import { Ihouse } from 'app/interfaces/Ihouse';
import { BsModalService } from 'ngx-bootstrap';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import { ModalService } from 'app/services/modal.service';
import { FormControlsService } from 'app/services/form-controls.service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-update-modal',
  templateUrl: './update-modal.component.html',
  styleUrls: ['./update-modal.component.css']
})
export class UpdateModalComponent implements OnInit {

  constructor(
      public fc: FormControlsService,
      public modalProp: ModalService,
      private houseService: HouseService,
      private modalService: BsModalService,
      private fb: FormBuilder,
      private route: ActivatedRoute,
     ) { }

    public Editor = ClassicEditor;

    selectedHouseImage: string = null;
    images: Array<string> = [];
 @Input() hId: Ihouse;

// Update Modal
@ViewChild('editTemplate') editmodal: TemplateRef<any>;

  @Input() house: Ihouse;

  ArrayOfImageNames = new Array<string>();
  ngOnInit() {

     // Initializing Update house properties
    this.fc._name = new FormControl('', [Validators.required, Validators.maxLength(50)]);
    this.fc._price = new FormControl('', [Validators.required, Validators.pattern(/^[1-9][0-9]{0,2}(?:\.?[0-9]{3}){0,3}(,[0-9]{2})?$/), Validators.min(0)]);
    this.fc._description = new FormControl('', [Validators.required]);
    this.fc._imageUrl = new FormControl('', []);
    this.fc._id = new FormControl();
    this.fc._latitude = new FormControl('', [Validators.required, Validators.pattern(/^[0-9]*\.?[0-9]*$/)]);
    this.fc._longitude = new FormControl('', [Validators.required, Validators.pattern(/^[0-9]*\.?[0-9]*$/)]);


    this.fc.updateForm = this.fb.group(
         {
             id : this.fc._id,
             name : this.fc._name,
             price : this.fc._price,
             description : this.fc._description,
             imageUrl : this.fc._imageUrl,
             latitude: this.fc._latitude,
             longitude: this.fc._longitude,
             available : true

         });
  }
  UploadFileNamesChange(arrayOfImages) {
    this.ArrayOfImageNames = arrayOfImages;
  }
  // Update an Existing Product
  onUpdate() {
    const editHouse = this.fc.updateForm.value;
    editHouse.imageUrl = this.ArrayOfImageNames.join(',');
    this.houseService.updateHouse(editHouse.id, editHouse).subscribe(
    result => {
        console.log('Product Updated');
        this.houseService.clearCache();
        this.modalProp.houses$ = this.houseService.getHouses();
        this.modalProp.houses$.subscribe(updatedlist => {
            this.modalProp.houses = updatedlist;

            this.modalProp.modalRef.hide();
           // this.rerender();
        });
    },
        error => console.log('Could Not Update Product')
    );
}

// Load the update Modal

onUpdateModal(houseEdit: Ihouse): void {
    this.fc._id.setValue(houseEdit.houseId);
    this.fc._name.setValue(houseEdit.name);
    this.fc._price.setValue(houseEdit.price);
    this.fc._description.setValue(houseEdit.description);
    this.fc._imageUrl.setValue(houseEdit.imageUrl);
    this.fc._latitude.setValue(houseEdit.latitude);
    this.fc._longitude.setValue(houseEdit.longitude);

    this.fc.updateForm.setValue({
        id : this.fc._id.value,
        name : this.fc._name.value,
        price :  this.fc._price.value,
        description : this.fc._description.value,
        imageUrl : this.fc._imageUrl.value,
        latitude: this.fc._latitude.value,
        longitude: this.fc._longitude.value,
        available : true
     });

    this.modalProp.modalRef = this.modalService.show(this.editmodal);

}



}
