import { Injectable, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';

@Injectable({
  providedIn: 'root'
})
export class DatatablesService {

  @ViewChild(DataTableDirective) dtElement: DataTableDirective;

 // Datatables Properties
 dtOptions: DataTables.Settings = {};
 dtTrigger: Subject<any> = new Subject();

  constructor() { }



  rerender() 
  {
      this.dtElement.dtInstance.then((dtInstance : DataTables.Api) => 
      {
          // Destroy the table first in the current context
          dtInstance.destroy();

          // Call the dtTrigger to rerender again
         this.dtTrigger.next();

      });
  }







}
