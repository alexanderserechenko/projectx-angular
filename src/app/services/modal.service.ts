import { Injectable } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { Ihouse } from 'app/interfaces/Ihouse';
import { Observable } from 'rxjs';
import { Icontact } from 'app/interfaces/icontact';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

 // Modal propertiesµ
 modalMessage: string;
 modalRef: BsModalRef;
 selectedHouse: Ihouse;
 houses$: Observable<Ihouse[]>;
 houses: Ihouse[] = [];
 contacts$:  Observable<Icontact[]>;
 contacts: Icontact[] = [];
 userRoleStatus: string;



  constructor() { }
}
