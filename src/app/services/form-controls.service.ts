import { Injectable } from '@angular/core';
import { FormGroup, FormControl, FormArray } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class FormControlsService {



// For the FormControl - Adding houses
insertForm: FormGroup;
name: FormControl;
price: FormControl;
description: FormControl;
imageUrl: FormControl;
longitude: FormControl;
latitude: FormControl;

// Updating the House
 updateForm: FormGroup;
_name: FormControl;
_price: FormControl;
_description: FormControl;
_imageUrl: FormControl;
_id: FormControl;
_longitude: FormControl;
_latitude: FormControl;

 // Deleting the House
   deleteForm: FormGroup;
   nameToDelete: FormControl;
   priceToDelete: FormControl;
   descriptionToDelete: FormControl;
   imageTodelete: FormControl;
   idToDelete: FormControl;
   longitudeToDelete: FormControl;
   latitudeTodelete: FormControl;

// Contact form
contactForm: FormGroup;
nameClient: FormControl;
emailClient: FormControl;
messageClient: FormControl;



  constructor() { }
}
