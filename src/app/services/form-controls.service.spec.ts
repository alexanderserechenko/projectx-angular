import { TestBed } from '@angular/core/testing';

import { FormControlsService } from './form-controls.service';

describe('FormControlsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FormControlsService = TestBed.get(FormControlsService);
    expect(service).toBeTruthy();
  });
});
