import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Icontact } from 'app/interfaces/icontact';
import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  constructor(private http: HttpClient) { }

  private baseUrl = 'https://localhost:5001/api/contact/getcontact';
  private contactUrl = 'https://localhost:5001/api/contact/createcontact';
  private deleteUrl = 'https://localhost:5001/api/contact/deletecontact/';


  private contact$: Observable<Icontact[]>;

  getContact(): Observable<Icontact[]> {
    // If client has deleted cache => get houses else return from cache
    if (!this.contact$) {
      // shareReplay() = next time when client requests getHouses() shareReplay gets it from cache
     this.contact$ = this.http.get<Icontact[]>(this.baseUrl).pipe(shareReplay());
    }

    return this.contact$;
  }

  // Insert House
  insertContact(newContact: Icontact): Observable<Icontact> {
      // To add a new house to Db we use HttpPost method
      return this.http.post<Icontact>(this.contactUrl, newContact);
  }

  contactDelete( id: number): Observable<any> {
    return this.http.delete(this.deleteUrl + id);
  }

  clearCache() {
    this.contact$ = null;
  }
}
