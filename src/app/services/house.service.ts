import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Ihouse } from 'app/interfaces/Ihouse';
import { shareReplay, flatMap, first } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class HouseService {

  constructor(private http: HttpClient) { }

  private baseUrl = 'https://localhost:5001/api/house/gethouses';
  private houseUrl = 'https://localhost:5001/api/house/addhouse';
  private deleteUrl = 'https://localhost:5001/api/house/deletehouse/';
  private updateUrl = 'https://localhost:5001/api/house/updatehouse/';

  private house$: Observable<Ihouse[]>;

  getHouses(): Observable<Ihouse[]> {
    // If client has deleted cache => get houses else return from cache
    if (!this.house$) {
      // shareReplay() = next time when client requests getHouses() shareReplay gets it from cache
     this.house$ = this.http.get<Ihouse[]>(this.baseUrl).pipe(shareReplay());
    }

    return this.house$;
  }


  // Get House by its ID
  getHouseById( id: number): Observable<Ihouse> {
    // return houses list from cache that gethouses method has => flatMap returns observable type
    // result gets passed to the next method in the sequence => first() to get the first value of id we use first() or default()
     return this.getHouses().pipe(flatMap(result => result), first(house => house.houseId === id ));
  }

  // Insert House
  insertHouse(newHouse: Ihouse): Observable<Ihouse> {
      // To add a new house to Db we use HttpPost method
      return this.http.post<Ihouse>(this.houseUrl, newHouse);
  }


  // Udate house
  updateHouse(id: number, editHouse: Ihouse): Observable<Ihouse> {
      return this.http.put<Ihouse>(this.updateUrl +  id, editHouse);
  }


  // Delete House
  deleteHouse( id: number): Observable<any> {
    return this.http.delete(this.deleteUrl + id);
  }

  // Clear cache
  clearCache() {
    this.house$ = null;
  }

}
