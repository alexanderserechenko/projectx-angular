
import { Component, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpRequest, HttpEventType, HttpResponse } from '@angular/common/http';

import { Uploader } from '../entities/uploader';
import { UploadQueue } from '../entities/uploadqueue';
import { all } from 'q';



@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent  {


  // getter : get overall progress
  get progress(): number {
    let psum = 0;

    for (const entry of this.uploader.queue) {
      psum += entry.progress;
    }

    if (psum === 0) {
      return 0;
    }

    return Math.round(psum / this.uploader.queue.length);
  }

  constructor(private http: HttpClient) {
    this.message = '';
  }

  @Output() UploadFileNamesChange = new EventEmitter<Array<String>>();
  public message: string;
  public uploader: Uploader = new Uploader();


    UploadedFileNames: Array<string> = [];

  onFilesChange(fileList: Array<File>) {
    for (const file of fileList) {
      this.uploader.queue.push(new UploadQueue(file));
    }
  }



  onSelectChange(event: EventTarget) {
    const eventObj: MSInputMethodContext = event as MSInputMethodContext;
    const target: HTMLInputElement = eventObj.target as HTMLInputElement;
    const files: FileList = target.files;
    const file = files[0];
    if (file) {
      this.uploader.queue.push(new UploadQueue(file));
      // console.log(file);
      console.log('Total Count:' + this.uploader.queue.length);
    }

  }
    upload(id) {
    if (id == null) {
            return;
    }
    const selectedFile = this.uploader.queue.find(s => s.id === id);
    if (selectedFile) {
        const formData = new FormData();
        formData.append(selectedFile.file.name, selectedFile.file);

        const uploadReq = new HttpRequest('POST', `https://localhost:5001/api/upload`, formData, {
          reportProgress: true,
        });

        this.http.request(uploadReq).subscribe(event => {
          if (event.type === HttpEventType.UploadProgress) {
            selectedFile.progress = Math.round(100 * event.loaded / event.total);
          } else if (event.type === HttpEventType.Response) {
            selectedFile.message = event.body.toString();
            this.UploadedFileNames.push(event.body.toString());
            this.UploadFileNamesChange.next(this.UploadedFileNames);
            // console.warn(event.body.toString());
          }
        });
      }
  }
  // upload all selected files to server
  uploadAll() {
    // find the remaning files to upload
    const remainingFiles = this.uploader.queue.filter(s => !s.isSuccess);
    for (const item of remainingFiles) {
      this.upload(item.id);
    }
    // this.UploadFileNamesChange.next(remainingFiles.map(x => x.id));

  }

  // cancel all
  cancelAll() {
    this.uploader.queue.splice(0);
    // Todo: Delete all files from the uploadfilenames array
  }

  cancel(id) {


    this.uploader.queue.splice(id, 1);

  }

}
