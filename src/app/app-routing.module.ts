
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HouseListComponent } from './houses/house-list/house-list.component';

import { AuthGuardService } from './guards/auth-guard.service';

import { HouseDetailsComponent } from './houses/house-details/house-details.component';
import { AdminComponent } from './admin/admin.component';
import { MessagesComponent } from './messages/messages.component';





@NgModule({
  imports:  [RouterModule.forRoot([
    { path: 'home', component: HouseListComponent },
    { path: 'login', component: LoginComponent  },
    { path: 'admin', component: AdminComponent, canActivate: [AuthGuardService] },
    { path: 'messages', component: MessagesComponent, canActivate: [AuthGuardService]  },
    { path: 'register', component: RegisterComponent  },


    // { path: 'access-denied', component: AccessDeniedComponent   },
    { path: 'houses/:id', component: HouseDetailsComponent   },
    { path: '**', redirectTo: '/home'},
    // If you try to access pages that don't exist it wil redirect to Home page
  ])],

  exports: [ RouterModule],
  providers: [AuthGuardService]
})
export class AppRoutingModule {}