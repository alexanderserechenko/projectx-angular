export interface Icontact {
    contactId?: number;
    name: string;
    email: string;
    message: string;
}