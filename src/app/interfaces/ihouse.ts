export interface Ihouse {
    houseId?: number;
    name: string;
    description: string;
    available: boolean;
    price: number;
    imageUrl: string;
    latitude: number;
    longitude: number;
}