import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AccountService } from 'app/services/account.service';
import { HouseService } from 'app/services/house.service';
import { ModalService } from 'app/services/modal.service';



@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private acct: AccountService, private houseService: HouseService, public modalProp: ModalService) { }

  loginStatus$: Observable<boolean>;
  UserName$: Observable<string>;


  ngOnInit() {

    this.loginStatus$ = this.acct.isLoggedIn;

    this.UserName$ = this.acct.currentUserName;

  }

  onLogOut() {
    this.houseService.clearCache();
    this.acct.logout();
  }

}
