import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HouseService } from 'app/services/house.service';
import { Ihouse } from 'app/interfaces/Ihouse';

@Component({
  selector: 'app-house-details',
  templateUrl: './house-details.component.html',
  styleUrls: ['./house-details.component.css']
})
export class HouseDetailsComponent implements OnInit {
  
  selectedHouseImage: string = null;
  images: Array<string> = [];
  @Input() house: Ihouse;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private houseService: HouseService) { }

  ngOnInit() {
    /* tslint:disable:no-string-literal */
    let id = + this.route.snapshot.params['id'];
    this.houseService.getHouseById(id)
      .subscribe(result => {
        this.house = result;
        this.images = this.house.imageUrl.split(',').map(imageUrl => 'https://localhost:5001/upload/' + imageUrl);
        this.selectedHouseImage = this.images[0];
      });
  }
}
