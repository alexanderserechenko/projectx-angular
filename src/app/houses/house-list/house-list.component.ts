import { Component, OnInit, ViewChild, TemplateRef, ChangeDetectorRef, OnDestroy, Input } from '@angular/core';
import {  Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { HouseService } from 'app/services/house.service';
import { AccountService } from 'app/services/account.service';

import { ActivatedRoute } from '@angular/router';
import { Ihouse } from 'app/interfaces/Ihouse';
import { ModalService } from 'app/services/modal.service';
import { DatatablesService } from 'app/services/datatables.service';
import { FormControlsService } from 'app/services/form-controls.service';



@Component({
  selector: 'app-house-list',
  templateUrl: './house-list.component.html',
  styleUrls: ['./house-list.component.scss']
})
    export class HouseListComponent  implements OnInit {


   @ViewChild(DataTableDirective) dtElement: DataTableDirective;


   @Input() house : Ihouse;
  constructor(
    private houseService: HouseService,
    private chRef: ChangeDetectorRef,
    private acct: AccountService,
    public modalProp: ModalService,
    public fc: FormControlsService,
    private route: ActivatedRoute, 
    private dtTrigger: DatatablesService ) { }


    

    ngOnInit() {
        this.modalProp.houses$ = this.houseService.getHouses();

        this.modalProp.houses$.subscribe(result => {
            this.modalProp.houses = result;

            this.chRef.detectChanges();

             this.dtTrigger.dtTrigger.next();
        });

        this.acct.currentUserRole.subscribe(result => {this.modalProp.userRoleStatus = result; });
    }


    public createImgPath = (serverPath: string) => {
      const sPath = serverPath.split(',');
      return `https://localhost:5001/upload/${sPath[0]}`;
    }

    goToDetails(){
       /* tslint:disable:no-string-literal */
      let id = + this.route.snapshot.params['id'];
      this.houseService.getHouseById(id).subscribe(result => this.house = result);
    }

}
