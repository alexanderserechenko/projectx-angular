import { Component, OnInit, Input } from '@angular/core';

import { Ihouse } from 'app/interfaces/Ihouse';
import { Router } from '@angular/router';
import { ModalService } from 'app/services/modal.service';

@Component({
  selector: 'app-house-select',
  templateUrl: './house-select.component.html',
  styleUrls: ['./house-select.component.css']
})
export class HouseSelectComponent implements OnInit {

  @Input() hId: Ihouse;

  constructor(
    public modalProp: ModalService,
    private router: Router,
    ) { }

  ngOnInit() {
  }

  onSelect(house: Ihouse): void {
    this.modalProp.selectedHouse = house;

    this.router.navigateByUrl('/houses/' + house.houseId);
}

}
