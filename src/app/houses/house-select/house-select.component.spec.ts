import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HouseSelectComponent } from './house-select.component';

describe('HouseSelectComponent', () => {
  let component: HouseSelectComponent;
  let fixture: ComponentFixture<HouseSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HouseSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HouseSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
