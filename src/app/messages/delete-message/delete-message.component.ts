import { Component, OnInit, Input, ViewChild, TemplateRef } from '@angular/core';
import { Icontact } from 'app/interfaces/icontact';
import { FormControlsService } from 'app/services/form-controls.service';
import { ModalService } from 'app/services/modal.service';
import { BsModalService } from 'ngx-bootstrap';
import { ContactService } from 'app/services/contact.service';

@Component({
  selector: 'app-delete-message',
  templateUrl: './delete-message.component.html',
  styleUrls: ['./delete-message.component.scss']
})
export class DeleteMessageComponent implements OnInit {

  constructor(
    public fc: FormControlsService,
    public modalProp: ModalService,
    private contactService: ContactService,
    private modalService: BsModalService
  ) { }
  @ViewChild('deleteMessage') deleteMessage: TemplateRef<any>;
  @Input() cId: Icontact;


  ngOnInit() {
  }



   // Method to Delete the house
   onDelete(): void {
    const deleteContact = this.cId;

    this.contactService.contactDelete(deleteContact.contactId).subscribe(result => {
    this.contactService.clearCache();
    this.modalProp.contacts$ = this.contactService.getContact();
    this.modalProp.contacts$.subscribe(newlist => {
            this.modalProp.contacts = newlist;
            this.modalProp.modalRef.hide();
           // this.rerender();
        });
    });
}

onDeleteMessage(contactDelete: Icontact): void {
    this.modalProp.modalRef = this.modalService.show(this.deleteMessage);
}

}
