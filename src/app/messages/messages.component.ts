import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ContactService } from 'app/services/contact.service';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { ModalService } from 'app/services/modal.service';
import { AccountService } from 'app/services/account.service';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();


@ViewChild(DataTableDirective) dtElement: DataTableDirective;

  constructor(
    public contactService: ContactService,
    public modalProp: ModalService,
    private chRef: ChangeDetectorRef,
    private acct: AccountService,
  ) {}

  ngOnInit() {

    this.dtOptions = {
      paging: false,
      autoWidth: true,
      lengthChange: false,
      searching: false,

      };

    this.modalProp.contacts$ = this.contactService.getContact();
    this.modalProp.contacts$.subscribe(result => {

        this.modalProp.contacts = result;
        this.chRef.detectChanges();
        this.dtTrigger.next();

  });

  this.acct.currentUserRole.subscribe(result => {this.modalProp.userRoleStatus = result});


}

}
